var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

describe("Pruebas sencillas", function() {

it('Test suma', function() {
  expect(9+4).to.equal(13);
});
});


describe("Pruebas red", function() {

it('Test internet', function (done) {
  request.get("http://www.as.com",
  function (error, response, body) {
expect(response.statusCode).to.equal(200);
done();
  });
  });

  it('Test puerto', function (done) {
    request.get("http://localhost:8081",
    function (error, response, body) {
  expect(response.statusCode).to.equal(200);
  done();
    });
    });

    it('Test body1', function (done) {
      request.get("http://localhost:8081",
      function (error, response, body) {
    expect(body).contains('BLOG');
    done();
      });
      });
});



describe  ("Test contenido HTML", function(done) {
  it('Test H1', function() {
    request.get("http://localhost:8081",
    function( error, response, body){
      console.log(body);
    expect($('body h1')).to.have.text("Bienvenido a mi BLOG");
  });
    });
  });
