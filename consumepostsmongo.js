const URL = "https://api.mlab.com/api/1/databases/ricardo/collections/posts?apiKey=g6VpUkYz5yXk8CoSjb6g3d8er3vdugQi";
var response;

function obtenerPosts() {

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  console.log(response);
  mostrarPosts();
};

function mostrarPosts() {
  var tabla = document.getElementById("tablaPosts");
  for (var i = 0; i < response.length; i++) {
    //alert(response[i].titulo);
    var fila = tabla.insertRow(i+1);
    var celdaId = fila.insertCell(0);
    var celdaTitulo = fila.insertCell(1);
    var celdaTexto = fila.insertCell(2);
    var celdaAutor = fila.insertCell(3);

    celdaId.innerHTML = response[i]._id.$oid;
    celdaTitulo.innerHTML = response[i].titulo;
    celdaTexto.innerHTML = response[i].texto;
    if (response[i].autor != undefined)
    {
      celdaAutor.innerHTML = response[i].autor.nombre + " " + response[i].autor.apellido;
    }
    else
    {
      celdaAutor.innerHTML = "Anónimo";
    }

  }
}
